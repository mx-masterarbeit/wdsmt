.. Wikidata Semantic Measure Toolkit documentation master file, created by
   sphinx-quickstart on Fri Dec 11 14:13:42 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Wikidata Semantic Measure Toolkit's documentation!
=============================================================

.. automodule:: wdsmt.classes.SemanticMeasureToolkit
    :members:


.. toctree::
   :maxdepth: 2
   :caption: Contents:




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
