# -*- coding: utf-8 -*-

import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
module_path = os.path.abspath(os.path.join(".."))
if module_path not in sys.path:
    sys.path.append(module_path)

# Import Libraries
import asyncio
import numpy as np

# Imports from Toolkit
# from wdsmt.classes.Concepts import Concepts, inf_concept
from wdsmt.classes.InformationContent import InformationContent
from wdsmt.classes.DirectGroupwiseSimilarity import DirectGroupwiseSimilarity

# Imports helper for access to idea annotations
from ConceptAccessor import ConceptAccessor

predicates = ["wdt:P31", "wdt:P279"]
endpoint = "wikidata"

# load ideas
ideas = ConceptAccessor("annotated_ideas.yaml")

async def main():
    dgs = DirectGroupwiseSimilarity(predicates, endpoint)
    IC = InformationContent(predicates, endpoint)

    U = ideas.get_concepts(4)
    V = ideas.get_concepts(1)

    print("Feature Based Groupwise Measures")
    for direct_groupwise_function in ['term_overlap', 'normalized_term_overlap']:
        print(f"{direct_groupwise_function}:", end="")
        print(await getattr(dgs, direct_groupwise_function)(U, V))

    print("Information Based Groupwise Measures")
    print("group_IC_naive:", end="")
    print(await getattr(dgs, "group_IC")(U, V, IC.IC_naive))
    print("group_IC_log_naive:", end="")
    print(await getattr(dgs, "group_IC")(U, V, IC.IC_log_naive))

asyncio.run(main())
