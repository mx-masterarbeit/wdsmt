import asyncio

from collections import namedtuple

from aiocache import cached, Cache
from aiocache.serializers import PickleSerializer

Result = namedtuple('Result', "content, status")


@cached(
    ttl=10, cache=Cache.REDIS, key="key", serializer=PickleSerializer(),
    port=6379, namespace="main")
async def cached_call():
    print("Sleeping for three seconds zzzz.....")
    await asyncio.sleep(3)
    return Result("content", 200)


async def test_cached():
    cache = Cache(Cache.REDIS, endpoint="127.0.0.1", port=6379, namespace="main")
    result = await cached_call()
    assert await cache.exists("key") is True
    # test = await cache.get("key")
    # print(test)
    await cache.delete("key")
    await cache.close()
    return result


if __name__ == "__main__":
    print(asyncio.run(cached_call()))
    print(asyncio.run(cached_call()))
    print(asyncio.run(test_cached()))


