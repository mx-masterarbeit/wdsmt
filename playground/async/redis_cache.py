from redis import StrictRedis

class CacheAccessor:
    """docstring for CacheAccessor"""
    def __init__(self, host='localhost', port='6379'):
        self.cache = StrictRedis(host, port, charset="utf-8", decode_responses=True)

    def delete_concept(self, concept):
        return self.cache.delete(*cache.keys(f"*'{concept}'*"))
