def number_of_nodes(predicates, root="wd:q35120"):
    """
    Retruns the Query
    """
    return f"""
    SELECT (COUNT(DISTINCT ?subject) AS ?numberOfNodes)
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
    WHERE {{
        ?subject ({"|".join(predicates)})* {root}.
    }}
    """

def number_of_leaves(predicates, root):

    # The nonexistent outgoing edges are realized as a union
    union = []
    for predicate in predicates:
        union.append("{ [] predicate ?subject. }")

    return f"""
    SELECT (COUNT(DISTINCT ?subject) AS ?numberOfLeaves) WHERE {{
      ?subject (({"|".join(predicates)})*) {root}.
      SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
      FILTER(NOT EXISTS {{
        {" UNION ".join(union)}
      }})
    }}
    """

def direct_ancestors(item, predicates):
    predicates_nl = "\n".join(predicates)
    return f"""
    SELECT ?item ?itemLabel ?pre ?ancestor ?ancestorLabel
    WHERE {{
        VALUES ?item {{
            {item}
        }}
    VALUES ?pre {{
            {predicates_nl}
    }}
    ?item ?pre ?ancestor.
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
    }}
    """

def number_of_ancestors(item, predicates):
    predicates_nl = "\n".join(predicates)
    return f"""
    SELECT (COUNT ?ancestor as numberOfAncestors)
    WHERE {{
        VALUES ?item {{
            {item}
        }}
    VALUES ?pre {{
            {predicates_nl}
    }}
    ?item ?pre ?ancestor.
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
    }}
    """


def decendents(item, predicates):
    predicates_nl = "\n".join(predicates)
    return f"""
    SELECT ?item ?itemLabel ?pre ?decendent ?decendentLabel
    WHERE {{
        VALUES ?item {{
            {item}
        }}
    VALUES ?pre {{
            {predicates_nl}
    }}
    ?decendent ?pre ?item.
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
    }}
    """

def number_of_decendents(item, predicates):
    predicates_nl = "\n".join(predicates)
    return f"""
    SELECT (COUNT ?decendent as numberOfDecendents)
    WHERE {{
        VALUES ?item {{
            {item}
        }}
    VALUES ?pre {{
            {predicates_nl}
    }}
    ?decendent ?pre ?item.
    SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
    }}
    """
