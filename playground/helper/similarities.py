import numpy as np

from contextlib import suppress

from SPARQLWrapper import SPARQLWrapper, JSON

headers = { 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

def A(concept, predicates):
    """
    Returns the set of ancestors of a concept going up to the root element.
    Excluding the concept itself.
    """
    predicates_nl = "\n".join(predicates)
    predicates_inline = " | ".join(predicates)
    query = f"""
        SELECT DISTINCT ?ancestor
        WHERE {{
        VALUES ?pre {{
            {predicates_nl}
        }}
        {concept}  ({predicates_inline})* ?item.
        ?item ?pre ?ancestor.
        }}
    """

    sparql = SPARQLWrapper("http://query.wikidata.org/sparql", agent = headers["User-Agent"])
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    ancestors = [ancestor['ancestor']['value'] for ancestor in results["results"]["bindings"]]
    with suppress(ValueError, AttributeError):
        ancestors.remove(c.replace("wd:", "http://www.wikidata.org/entity/"))

    return np.array(ancestors)

def D(concept, predicates):
    """
    Returns the set of decendents of a concept going down to the leaves.
    Excluding the concept itself.
    """
    predicates_nl = "\n".join(predicates)
    predicates_inline = " | ".join(predicates)
    query = f"""
    SELECT DISTINCT ?decendent
    WHERE {{
      VALUES ?pre {{ {predicates_nl} }}
      ?item ({predicates_inline})* {concept}.
      ?decendent ?pre ?item.
    }}
    """

    sparql = SPARQLWrapper("http://query.wikidata.org/sparql", agent = headers["User-Agent"])
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    decendents = [decendent['decendent']['value'] for decendent in results["results"]["bindings"]]
    with suppress(ValueError, AttributeError):
        decendents.remove(concept.replac    e("wd:", "http://www.wikidata.org/entity/"))

    return np.array(decendents)


def CMatch(u, v, predicates):
    """
    CMatch is decscribed by:
    .. math::
        \mathit{sim}_\text{CMatch}(u,v) = \frac{|A(u) \cap A(v)|}{|A(u) \cup A(v)|}

    with A(c) as the set of ancestors of c going up to the root element.
    """

    A_u, A_v = A(u, predicates), A(v, predicates)

    union = np.union1d(A_u, A_v)
    intersection = np.intersect1d(A_u, A_v)

    return len(intersection) / len(union)

def Bulskov(u, v, predicates, alpha=0.5):
    """
    Bulskov-Similarity between u, v is decscribed by:
    .. math::
        \mathit{sim}_\text{Bulskov}(u,v) = \alpha\frac{|A(u) \cup A(v)|}{|A(u)|} + (1 - \alpha)\frac{|A(u) \cup A(v)|}{|A(v)|}

    with A(c) as the set of ancestors of c going up to the root element.
    """

    A_u, A_v = A(u, predicates), A(v, predicates)
    union = np.union1d(A_u, A_v)

    return alpha * (len(union)/len(A_u)) + (1 - alpha) * (len(union)/len(A_v))

def dist_Sanchez(u, v):
    A_u, A_v = A(u, predicates), A(v, predicates)

    diff_u_v, diff_v_u = len(np.setdiff1d(A_u, A_v)), len(np.setdiff1d(A_v, A_u))
    intersection = len(np.intersect1d(A_u, A_v))

    dist = 1 + ((diff_u_v + diff_v_u) / (diff_u_v + diff_v_u + intersection))

    return np.log2(dist)


def IC_APSx(c, predicates):
    """
    Derived from the a-priori-score mentioned in \\cite{SchickelZuber2007OSS} this Information Content Score is now between (0, 1]
    .. math::
        \mathit{IC}_\text{APSx}(c) = \frac{1}{|D(c)| + 1}

    with D(c) as the set of decendents of c.
    """
    return 1 / (len(D(c, predicates)) + 1)

def IC_naive(c, predicates, alpha=0.5):
    """
    Inspired by the a-priori-score mentioned in \\cite{SchickelZuber2007OSS} and Similarity Measures using ancestors this
    algorithm combines ancestors and decendents to give naive information content score as a fallback.

    .. math::
        \mathit{IC}_\text{naive}(c) = \alpha \cdot \left(1-\frac{1}{|A(c)| + 1}\left) + (1-\alpha) \cdot \frac{1}{|D(c)| + 1}

    with A(c) as the set of ancestors and D(c) as the set of decendents of c.
    """

    ancestor_part = 1 - (1 / (len(A(c, predicates)) + 1))
    decendent_part = 1 / (len(D(c, predicates)) + 1)

    return alpha * ancestor_part + (1-alpha) * decendent_part

def MICA(u, v, predicates, ic=IC_naive):
    """
    Finds the Most Informative Common Ancestor (MICA) of concept u and v.

    Returns a tuple containing the MICA-element and its score
    """
    A_u, A_v = A(u, predicates), A(v, predicates)

    intersection = np.intersect1d(A_u, A_v)

    #TODO: parallelize
    mica = []
    for common_ancestor in intersection:
        mica.append((common_ancestor, ic(common_ancestor)))

    return max(mica,key=itemgetter(1))

