# Wikidata Semantic Measure Toolkit (wdsmt)

The wikidata semantic measure toolkit, wdsmt for short, offers serveral functions to retrieve semantic measures from wikidata.
Even though it is only developed and tested for wikidata it should be possible (or easy enough) to use it with other knowledge bases.

## Development Environment

The wdsmt was developed using [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/).
This readme expects that [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) is available.
To make working with wikidata viable I use a redis-cache. Therefore it is needed to install redis and run it.

### Set Up

```sh
cd path/to/this/library
workon wdsmt
pip install -r requirements.txt
python setup.py install --user
```

#### Install Redis

Because I'm on macOS im just listing the steps I took. See the [official website](https://redis.io/download) for more information.

```sh
brew update
brew install redis
```

To run the local redis-server run `redis-server /usr/local/etc/redis.conf`

### Use Playground (jupyter lab)

```sh
cd ./playground
jupyter lab
```

### Run Tests

The tests expect wdsmt to be installed as a package.
To run the tests use:

```sh
python -m unittest discover -s ./tests
```

Test the self defined datatypes
```sh
python -m unittest tests.testConceptsDataType
```

Test the Utility Class
```sh
python -m unittest tests.testUtilityClass
```
