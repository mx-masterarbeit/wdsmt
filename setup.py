import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="wdsmt", # Replace with your own username
    version="0.0.2",
    author="Maximilian Stauss",
    author_email="max.stauss@fu-berlin.de",
    description="Toolkit to run semantic measurements on wikidata and other knowledge bases",
    long_description=long_description,
    long_description_content_type="text/markdown",
    # url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Development Status :: 3 - Alpha"
    ],
    python_requires='>=3.8',
)
