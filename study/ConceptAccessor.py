import yaml


class ConceptAccessor:
    def __init__(self, idea_dictionary, is_path=True):
        if is_path:
            with open(idea_dictionary, "r") as stream:
                try:
                    self.idea_dictionary = yaml.safe_load(stream)
                except yaml.YAMLError as exc:
                    print(exc)
        else:
            self.idea_dictionary = idea_dictionary

    def __len__(self):
        return len(self.idea_dictionary)

    def get_ideas(self):
        return [x['id'] for x in self.idea_dictionary]

    def get_concepts(self, idea_no):
        return [
            x["concept"]
            for x in self.idea_dictionary[idea_no - 1]["metadata"]["concepts"]
        ]

    def get_topics(self, idea_no):
        return [
            x["concept"]
            for x in self.idea_dictionary[idea_no - 1]["metadata"]["concepts"]
        ]

    def get_idea_by_uuid(self, uuid):
        for idea in self.idea_dictionary:
            if idea['innovonto_uuid'] == uuid:
                return idea
        return None

    def pos(self, idea_id):
        return int(idea_id.split('_')[1])
