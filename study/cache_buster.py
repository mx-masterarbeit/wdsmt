import os
import sys

module_path = os.path.abspath(os.path.join(".."))
if module_path not in sys.path:
    sys.path.append(module_path)

from functools import reduce

import asyncio
import numpy as np
import yaml

from redis import StrictRedis
import pickle


from wdsmt.classes.Concepts import Concepts, inf_concept
from wdsmt.classes.Utility import Utility
from wdsmt.classes.InformationContent import InformationContent
from wdsmt.classes.InformationBasedSemanticMeasure import (
    InformationBasedSemanticMeasure,
)
from wdsmt.classes.IndirectGroupwiseSimilarity import IndirectGroupwiseSimilarity


class CacheAccessor:
    """docstring for CacheAccessor"""

    def __init__(self, host="localhost", port="6379"):
        self.cache = StrictRedis(host, port, charset="utf-8", decode_responses=True)

    def delete_concept(self, concept):
        return self.cache.delete(*cache.keys(f"*'{concept}'*"))

cache_accessor = CacheAccessor()

cache = StrictRedis("localhost", "6379", charset="utf-8")

# count = 0
# for key in cache.scan_iter():
#     value = pickle.loads(cache.get(key))
#     if value == []:
#         cache.delete(key)
#         count += 1

print(cache_accessor.delete_concept("wd:Q3711790"))
