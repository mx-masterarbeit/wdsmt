# -*- coding: utf-8 -*-

import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
module_path = os.path.abspath(os.path.join(".."))
if module_path not in sys.path:
    sys.path.append(module_path)

import asyncio
import numpy as np
from inspect import getmembers, isfunction

from wdsmt.classes.Concepts import Concepts, inf_concept
# from wdsmt.classes.Utility import Utility
from wdsmt.classes.InformationContent import InformationContent
from wdsmt.classes.InformationBasedSemanticMeasure import (
    InformationBasedSemanticMeasure,
)
from wdsmt.classes.FeatureBasedSemanticMeasure import FeatureBasedSemanticMeasure
from wdsmt.classes.IndirectGroupwiseSimilarity import IndirectGroupwiseSimilarity

from ConceptAccessor import ConceptAccessor

predicates = ["wdt:P31", "wdt:P279"]
endpoint = "wikidata"

# load ideas
ideas = ConceptAccessor("all_ideas.yaml")

def get_sim_funtions(mod):
    return [
        o[0]
        for o in getmembers(mod)
        if isfunction(o[1]) and o[0].startswith("sim")
    ]
