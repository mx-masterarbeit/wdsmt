"""
This script is here to find annoted concepts that are not connected to the root.
"""

from context import *
from wdsmt.classes.GraphAccessor import GraphAccessor

async def main():

    idea_set = set([])
    for i in range(len(ideas.get_ideas())):
        concepts = ideas.get_concepts(i+1)
        topics = ideas.get_topics(i+1)

        idea_set.update(concepts, topics)

    idea_set = list(idea_set)

    for idea in idea_set:
        print(idea, end=": ")

        async with GraphAccessor(predicates, endpoint) as graph:
            ancestors = await graph.ancestors(idea, inclusive=True)
            print('len -', ancestors.length(), end=" ")
            if 'wd:Q35120' not in ancestors.concept_list:
                print("!! NO ROOT")
            else:
                print("")

asyncio.run(main())
