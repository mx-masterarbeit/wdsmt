import json
from operator import itemgetter

import asyncio
import numpy as np
from aiosparql.client import SPARQLClient, SPARQLRequestFailed

from aiocache import cached, Cache
from aiocache.serializers import PickleSerializer

import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../..")))
from wdsmt.classes import (
    GraphAccessor,
    InformationContent,
    InformationBasedSemanticMeasure,
)

entity = "wd:Q35120"
house_cat = "wd:Q146"
dog = "wd:Q144"
sphynx = "wd:Q42712"

information_content = InformationContent(
    predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata"
)
ic_log = information_content.IC_log_naive


async def test_graph_accessor():
    async with GraphAccessor(
        predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata"
    ) as graph:
        [result1, result2] = await asyncio.gather(
            graph.ancestor_count(entity), graph.decendent_count(entity)
        )
        print(result1, result2)


async def test_information_content():
    information_content = InformationContent(
        predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata"
    )
    ic = information_content.IC_naive
    ic_log = information_content.IC_log_naive

    ic_cat = await ic(house_cat)
    print("IC_naive from Cat:", ic_cat)

    ic_log_cat = await ic_log(house_cat)
    print("IC_log_naive from Cat:", ic_log_cat)

    ic_entity = await ic_log(entity)
    print("IC_log_naive from entity:", ic_entity)


async def get_ic(concept):
    information_content = InformationContent(
        predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata"
    )
    ic_log = information_content.IC_log_naive

    concept_ic = await ic_log(concept)
    print(f"IC_log_naive of {concept}: {concept_ic}")


async def test_cache():
    async with GraphAccessor(
        predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata"
    ) as graph:
        [result] = await asyncio.gather(graph.decendent_count(entity))
    return result


async def gather_ic_tuples(*ancestors, limit=5):
    semaphore = asyncio.Semaphore(limit)

    async def get_ic_tuple(common_ancestor):
        async with semaphore:
            return (common_ancestor, await ic_log(common_ancestor))

    return await asyncio.gather(
        *(get_ic_tuple(ancestor) for ancestor in ancestors), return_exceptions=True
    )


async def test_minimal_mica():
    async with GraphAccessor(
        predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata"
    ) as graph:
        [A_dog, A_cat] = await asyncio.gather(
            graph.ancestors(dog), graph.ancestors(house_cat)
        )
        intersection = A_dog.intersection(A_cat).concept_list

        semaphore = asyncio.Semaphore(5)

        ica = await gather_ic_tuples(*intersection)
        mica = max(ica, key=itemgetter(1))
        print({"value": mica[0], "ic_score": mica[1]})


async def test_MICA_in_class(u, v):
    info_measure = InformationBasedSemanticMeasure(
        predicates=["wdt:P31", "wdt:P279"],
        endpoint="wikidata",
        ic_function="IC_log_naive",
    )

    print(await info_measure.MICA(u, v))


# print(clear_cache_ns("semantic"))
asyncio.run(
    test_MICA_in_class(dog, sphynx)
)  # {'value': 'wd:Q57814795', 'ic_score': 0.45591200841242346}
# asyncio.run(get_ic("wd:Q9081"))
