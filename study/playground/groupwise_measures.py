from context import *

import wdsmt.classes.IndirectGroupwiseSimilarity as group_sim


print(ideas.get_topics(1))


async def main():
    ibsm = InformationBasedSemanticMeasure(
        predicates=predicates, endpoint=endpoint, ic_function="IC_log_naive"
    )
    fbsm = FeatureBasedSemanticMeasure(predicates=predicates, endpoint=endpoint)

    u, v = "wd:Q146", "wd:Q144"  # house_cat, dog
    U = ideas.get_concepts(4)
    V = ideas.get_concepts(1)

    print(U)

    for indirect_groupwise_funtion in group_sim._():
        print(f"{indirect_groupwise_funtion}:")

        print("- Feature Based Semantic Measures:")
        for measure in get_sim_funtions(FeatureBasedSemanticMeasure):
            print(measure, end=": ")
            print(await getattr(group_sim, indirect_groupwise_funtion)(U, V, getattr(fbsm, measure)))

        print("\n- Information Based Semantic Measures:")
        for measure in get_sim_funtions(InformationBasedSemanticMeasure):
            print(measure, end=": ")
            print(await getattr(group_sim, indirect_groupwise_funtion)(U, V, getattr(ibsm, measure)))

        print("\n")

asyncio.run(main())
