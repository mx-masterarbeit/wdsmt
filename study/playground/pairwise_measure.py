import os
import sys

module_path = os.path.abspath(os.path.join(".."))
if module_path not in sys.path:
    sys.path.append(module_path)

import asyncio
import numpy as np

from wdsmt.classes.Concepts import Concepts, inf_concept
from wdsmt.classes.Utility import Utility
from wdsmt.classes.InformationContent import InformationContent
from wdsmt.classes.InformationBasedSemanticMeasure import (
    InformationBasedSemanticMeasure,
)
from wdsmt.classes.FeatureBasedSemanticMeasure import FeatureBasedSemanticMeasure
from wdsmt.classes.IndirectGroupwiseSimilarity import IndirectGroupwiseSimilarity

from ConceptAccessor import ConceptAccessor

from inspect import getmembers, isfunction

predicates = ["wdt:P31", "wdt:P279"]
endpoint = "wikidata"

util = Utility(predicates=predicates, endpoint=endpoint)
information_content = InformationContent(predicates=predicates, endpoint=endpoint)
ibsm = InformationBasedSemanticMeasure(
    predicates=predicates, endpoint=endpoint, ic_function="IC_log_naive"
)
fbsm = FeatureBasedSemanticMeasure(predicates=predicates, endpoint=endpoint)

# load ideas
ideas = ConceptAccessor("all_ideas.yaml")

topics = ideas.get_topics(1)



async def main():
    u, v = "wd:Q146", "wd:Q144"  # house_cat, dog

    print("Information Based Semantic Measures:")
    similarity_measure_list_ic = [
        o[0]
        for o in getmembers(InformationBasedSemanticMeasure)
        if isfunction(o[1]) and o[0].startswith("sim")
    ]
    for measure in similarity_measure_list_ic:
        print(measure, await getattr(ibsm, measure)(u, v))

    print("Feature Based Semantic Measures:")
    similarity_measure_list_feature = [
        o[0]
        for o in getmembers(FeatureBasedSemanticMeasure)
        if isfunction(o[1]) and o[0].startswith("sim")
    ]
    for measure in similarity_measure_list_feature:
        print(measure, end=": ")
        print(await getattr(fbsm, measure)(u, v))


# print(clear_cache_ns("semantic"))
asyncio.run(main())  # {'value': 'wd:Q57814795', 'ic_score': 0.45591200841242346}
# asyncio.run(get_ic("wd:Q9081"))
