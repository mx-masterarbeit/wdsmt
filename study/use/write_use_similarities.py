import tensorflow as tf
import tensorflow_hub as hub
import numpy as np
import os
import pandas as pd
import re
import csv
import yaml

from sklearn.metrics.pairwise import cosine_similarity as sk_cos_sim

module_url = "https://tfhub.dev/google/universal-sentence-encoder-large/5" #@param ["https://tfhub.dev/google/universal-sentence-encoder/4", "https://tfhub.dev/google/universal-sentence-encoder-large/5"]
model = hub.load(module_url)
print ("module %s loaded" % module_url)
def embed(input):
  return model(input)


class ConceptAccessor:
    def __init__(self, idea_dictionary, is_path=True):
        if is_path:
            with open(idea_dictionary, "r") as stream:
                try:
                    self.idea_dictionary = yaml.safe_load(stream)
                except yaml.YAMLError as exc:
                    print(exc)
        else:
            self.idea_dictionary = idea_dictionary

    def get_ideas(self):
        return [x['id'] for x in self.idea_dictionary]

    def get_concepts(self, idea_no):
        return [
            x["concept"]
            for x in self.idea_dictionary[idea_no - 1]["metadata"]["concepts"]
        ]

    def get_topics(self, idea_no):
        return [
            x["concept"]
            for x in self.idea_dictionary[idea_no - 1]["metadata"]["concepts"]
        ]

ideas = ConceptAccessor("../all_ideas.yaml")

idea_texts = [idea['content'] for idea in ideas.idea_dictionary]
embeddings = embed(idea_texts)

def dot_product_similarity(embeddings):
    return np.inner(embeddings, embeddings)

def cosine_similarity(embeddings):
    return sk_cos_sim(embeddings, embeddings)

def angular_similarity(embeddings):
    cos_sim = cosine_similarity(embeddings)
    return 1 - (np.arccos(cos_sim) / np.pi)

## write csv
base_folder='../_csv/'
idea_ids = ideas.get_ideas()
for use_sim_function in ['dot_product_similarity', 'cosine_similarity', 'angular_similarity']:
    curr_csv = base_folder + 'universal_sentence_encoder/' + use_sim_function + '.csv'
    curr_sim = eval(use_sim_function)(embeddings)
    with open(curr_csv, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)

        # write header:
        csvwriter.writerow(['use_' + use_sim_function] + idea_ids)

        # rows
        for i in range(len(idea_ids)):

            # row columns
            columns = [idea_ids[i]] + list(curr_sim[i])

            # write row
            csvwriter.writerow(columns)
