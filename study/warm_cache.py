import os
import sys

module_path = os.path.abspath(os.path.join(".."))
if module_path not in sys.path:
    sys.path.append(module_path)

from functools import reduce

import asyncio
import numpy as np
import yaml

from wdsmt.classes.Concepts import Concepts, inf_concept
from wdsmt.classes.Utility import Utility
from wdsmt.classes.InformationContent import InformationContent
from wdsmt.classes.InformationBasedSemanticMeasure import (
    InformationBasedSemanticMeasure,
)
from wdsmt.classes.IndirectGroupwiseSimilarity import IndirectGroupwiseSimilarity

## set defaults
predicates = ["wdt:P31", "wdt:P279"]
endpoint = "wikidata"

util = Utility(predicates=predicates, endpoint=endpoint)
information_content = InformationContent(predicates=predicates, endpoint=endpoint)
ibsm = InformationBasedSemanticMeasure(
    predicates=predicates, endpoint=endpoint, ic_function="IC_log_naive"
)


## todo: use GraphAccessor
# collect all wikidata concepts mentioned as concept or topic
idea_set = set([])
for annotation in annotated_ideas:
    concepts = set([x["concept"] for x in annotation["metadata"]["concepts"]])
    topics = set([x["concept"] for x in annotation["metadata"]["topics"]])

    idea_set.update(concepts, topics)

idea_set = list(idea_set)


async def gather_ancestors(*ideas):
    semaphore = asyncio.Semaphore(4)  # set semaphore limit to 4

    async def get_ancestors(idea):
        async with semaphore:
            ancestors = util.ancestors(idea)
            return ancestors.concept_list

    return await asyncio.gather(
        *(get_ancestors(idea) for idea in ideas), return_exceptions=True
    )


ancestor_idea_set = np.union1d(
    idea_set, reduce(np.union1d, await gather_ancestors(*idea_set))
)

# load all of them and their ancestors in cache
ic = information_content.IC_naive


async def gather_ic_tuples(*ancestors):
    semaphore = asyncio.Semaphore(4)  # set semaphore limit to 4

    async def get_ic_tuple(common_ancestor):
        async with semaphore:
            return (common_ancestor, await ic(common_ancestor))

    return await asyncio.gather(
        *(get_ic_tuple(ancestor) for ancestor in ancestors), return_exceptions=True
    )


await gather_ic_tuples(*ancestor_idea_set)
