from context import *

import csv

import wdsmt.classes.IndirectGroupwiseSimilarity as group_sim
from wdsmt.classes.DirectGroupwiseSimilarity import DirectGroupwiseSimilarity
IC = InformationContent(predicates, endpoint)

async def main():
    ibsm = InformationBasedSemanticMeasure(
        predicates=predicates, endpoint=endpoint, ic_function="IC_log_naive"
    )
    fbsm = FeatureBasedSemanticMeasure(predicates=predicates, endpoint=endpoint)

    u, v = "wd:Q146", "wd:Q144"  # house_cat, dog
    U = ideas.get_topics(0)
    V = ideas.get_topics(1)

    ## For every groupwise semantic measure:
    #
    #     GOAL  | idea_1 | idea_2 | idea_3
    #    idea_1 |   1.0  |  0.5   |  0.3
    #    idea_2 |   0.4  |  1.0   |  0.4
    #    idea_3 |   0.3  |  0.5   |  1.0
    #
    # -> possible combinations: [feature_based, information_based{log|naive}] x groupwise_measures

    base_folder='./_csv/_extended/'
    pairwise_measure_types = {'feature_based': fbsm, 'information_based': ibsm}
    idea_ids = ideas.get_ideas()

    ## for every indirect groupwise measure:
    #     for indirect_groupwise_function in group_sim.funtion_list():
    #         # Feature Based Semantic Measures:
    #         for measure in get_sim_funtions(FeatureBasedSemanticMeasure):
    #             # set csv name
    #             curr_csv = base_folder + 'indirect_feature_based/' + indirect_groupwise_function + '_' + measure + '.csv'
    #             with open(curr_csv, 'w', newline='') as csvfile:
    #                 csvwriter = csv.writer(csvfile)
    #
    #                 # write header:
    #                 csvwriter.writerow([indirect_groupwise_function + '_' + measure] + idea_ids)
    #
    #                 # rows
    #                 for i in range(len(idea_ids)):
    #                     U = ideas.get_concepts(i)
    #
    #                     # row columns
    #                     columns = [idea_ids[i]]
    #                     for j in range(len(idea_ids)):
    #                         V = ideas.get_concepts(j)
    #                         columns.append(await getattr(group_sim, indirect_groupwise_function)(U, V, getattr(fbsm, measure)))
    #
    #                     # write row
    #                     csvwriter.writerow(columns)

    # for every indirect groupwise information based measure:
    for indirect_groupwise_function in group_sim.funtion_list():
        # Information Based Semantic Measures:
        for measure in get_sim_funtions(InformationBasedSemanticMeasure):
            for ic_function in ['IC_naive', 'IC_log_naive', 'IC_aps']:  # ['IC_naive', 'IC_log_naive']:
                ibsm = InformationBasedSemanticMeasure(predicates=predicates, endpoint=endpoint, ic_function=ic_function)
                # set csv name
                curr_csv = base_folder + 'indirect_information_based/' + indirect_groupwise_function + '_' + measure + '_' + ic_function + '.csv'
                with open(curr_csv, 'w', newline='') as csvfile:
                    csvwriter = csv.writer(csvfile)

                    # write header:
                    csvwriter.writerow([indirect_groupwise_function + '_' + measure] + idea_ids)

                    # rows
                    for i in range(len(idea_ids)):
                        U = ideas.get_concepts(i) + ideas.get_topics(i)

                        # row columns
                        columns = [idea_ids[i]]
                        for j in range(len(idea_ids)):
                            V = ideas.get_concepts(j) + ideas.get_topics(j)
                            columns.append(await getattr(group_sim, indirect_groupwise_function)(U, V, getattr(ibsm, measure)))

                        # write row
                        csvwriter.writerow(columns)


    dgs = DirectGroupwiseSimilarity(predicates, endpoint)
#     # Feature Based Semantic Measures:
#     for direct_groupwise_function in ['term_overlap', 'normalized_term_overlap']:
#         # set csv name
#         curr_csv = base_folder + 'direct_feature_based/' + direct_groupwise_function + '.csv'
#         with open(curr_csv, 'w', newline='') as csvfile:
#             csvwriter = csv.writer(csvfile)
#
#             # write header:
#             csvwriter.writerow([direct_groupwise_function] + idea_ids)
#
#             # rows
#             for i in range(len(idea_ids)):
#                 U = ideas.get_concepts(i)
#
#                 # row columns
#                 columns = [idea_ids[i]]
#                 for j in range(len(idea_ids)):
#                     V = ideas.get_concepts(j)
#                     columns.append(await getattr(dgs, direct_groupwise_function)(U, V))
#
#                 # write row
#                 csvwriter.writerow(columns)
#
#
    # Information Bases Semantic Measures
#     for direct_groupwise_function in ['group_IC']:
#         for ic_function in ['IC_aps']:
#             # set csv name
#             curr_csv = base_folder + 'direct_information_based/' + direct_groupwise_function + '_' + ic_function + '.csv'
#             with open(curr_csv, 'w', newline='') as csvfile:
#                 csvwriter = csv.writer(csvfile)
#
#                 # write header:
#                 csvwriter.writerow([direct_groupwise_function] + idea_ids)
#
#                 # rows
#                 for i in range(len(idea_ids)):
#                     U = ideas.get_concepts(i)
#
#                     # row columns
#                     columns = [idea_ids[i]]
#                     for j in range(len(idea_ids)):
#                         V = ideas.get_concepts(j)
#                         columns.append(await getattr(dgs, direct_groupwise_function)(U, V, getattr(IC, ic_function)))
#
#                     # write row
#                     csvwriter.writerow(columns)

asyncio.run(main())
