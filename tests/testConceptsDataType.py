import unittest
import numpy as np
from wdsmt.classes.Concepts import Concepts

class TestConceptsDataType(unittest.TestCase):

    def setUp(self):
        self.infinite = Concepts.infinite()
        self.abc = Concepts(["A","B","C"])
        self.bcd = Concepts(["B","C", "D"])


    def test_length(self):
        self.assertEqual(self.infinite.length(), np.inf)
        self.assertEqual(self.abc.length(), 3)

    def test_union(self):
        self.bc = Concepts(["B","C"])
        self.abcd = Concepts(["A","B","C","D"])

        self.assertEqual(self.abc.union(self.bcd).length(), 4)
        self.assertEqual(self.abc.union(self.bcd), self.abcd)

        self.assertEqual(self.infinite.union(self.bcd).length(), np.inf)
        self.assertEqual(self.abc.union(self.infinite), Concepts.infinite())

    def test_intersection(self):
        bc = Concepts(["B","C"])

        self.assertEqual(self.abc.intersection(self.bcd).length(), 2)
        self.assertEqual(self.abc.intersection(self.bcd), bc)

        self.assertEqual(self.infinite.intersection(self.bcd).length(), 3)
        self.assertEqual(self.abc.intersection(self.infinite), self.abc)

    def test_setDiff(self):
        # If A and B are sets, the set difference of A and B is the set of elements in A but not in B.
        ## Regular Case - setdiff of abc and bcd -> expected A
        self.assertEqual(self.abc.setdiff(self.bcd), Concepts(["A"]))
        ## Regular Case - setdiff of bcd and abc -> expected D
        self.assertEqual(self.bcd.setdiff(self.abc), Concepts(["D"]))
        ## Regular Case - setdiff of abc and [] -> expected abc
        self.assertEqual(self.abc.setdiff(Concepts([])), self.abc)
        ## Regular Case - setdiff of [] and abc  -> expected []
        self.assertEqual(Concepts([]).setdiff(self.abc), Concepts([]))
        ## Regular Case - setdiff of [] and []  -> expected []
        self.assertEqual(Concepts([]).setdiff(Concepts([])), Concepts([]))


        ## EdgeCase - setdiff of infinite and abc -> ecpected infinite:
        self.assertEqual(self.infinite.setdiff(self.abc), self.infinite)
        ## EdgeCase - setdiff of abc and infinite -> ecpected []:
        self.assertEqual(self.abc.setdiff(self.infinite), Concepts([]))
        ## EdgeCase - setdiff of infinite and infinite -> ecpected []:
        self.assertEqual(self.infinite.setdiff(self.infinite), Concepts([]))




if __name__ == '__main__':
    unittest.main()
