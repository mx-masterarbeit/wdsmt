import unittest
import numpy as np
from wdsmt.classes.InformationBasedSemanticMeasure import InformationBasedSemanticMeasure

class TestInformationBasedSemanticMeasures(unittest.IsolatedAsyncioTestCase):

    def setUp(self):
        self.ibsm_naive = InformationBasedSemanticMeasure(predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata")
        self.ibsm_log = InformationBasedSemanticMeasure(predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata", ic_function="IC_log_naive")
        self.house_cat = "wd:Q146"
        self.egyptian_mau = "wd:Q7295"
        self.dog = "wd:Q144"
        self.tiger = "wd:Q19939"

    async def asyncSetUp(self):
        pass
        # pre heat cache
        # await self.ibsm_naive.MICA(self.house_cat, self.dog)


    async def test_MICA_naive(self):
        ibsm_naive = InformationBasedSemanticMeasure(predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata")
        ## Test that IC_naive is greater zero (for house_cat)
        # ic_score_cat = await ibsm_naive.ic(self.house_cat)
        # ic_score_dog = await ibsm_naive.ic(self.dog)
        # self.assertTrue(ic_score_cat > 0)
        # self.assertTrue(ic_score_dog > 0)

        mica = await ibsm_naive.MICA(self.house_cat, self.dog)
        print(mica)
        del(ibsm_naive)
        # self.assertTrue(mica[1] > 0.2)

    async def test_resnik_naive(self):
        ibsm_naive = InformationBasedSemanticMeasure(predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata")
        mica = await ibsm_naive.similarity_Resnik(self.house_cat, self.dog)

if __name__ == '__main__':
    unittest.main()
