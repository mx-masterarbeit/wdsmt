import unittest
import numpy as np
from wdsmt.classes.InformationContent import InformationContent

class TestInformationContentClass(unittest.IsolatedAsyncioTestCase):

    async def asyncSetUp(self):
        self.information_content = InformationContent(predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata")
        self.house_cat = "wd:Q146"
        self.egyptian_mau = "wd:Q7295"
        self.dog = "wd:Q144"
        self.tiger = "wd:Q19939"


    async def test_IC_naive(self):
        ic_score_cat = await self.information_content.IC_naive(self.house_cat)
        ic_score_egyptian_mau = await self.information_content.IC_naive(self.egyptian_mau)


        ## Test that IC_naive is greater zero (for house_cat)
        self.assertTrue(ic_score_cat > 0)

        ## Test that egyptian_mau has a higher IC than house_cat
        self.assertTrue(ic_score_egyptian_mau > ic_score_cat)


    async def test_IC_log_naive(self):
        ic_score_cat = await self.information_content.IC_log_naive(self.house_cat)
        ic_score_egyptian_mau = await self.information_content.IC_log_naive(self.egyptian_mau)


        ## Test that IC_naive is greater zero (for house_cat)
        self.assertTrue(ic_score_cat > 0)

        ## Test that egyptian_mau has a higher IC than house_cat
        self.assertTrue(ic_score_egyptian_mau > ic_score_cat)


if __name__ == '__main__':
    unittest.main()
