import unittest
import numpy as np
from wdsmt.classes.Utility import Utility
from wdsmt.classes.Concepts import inf_concept, Concepts

class TestUtilityClass(unittest.TestCase):

    def setUp(self):
        self.util = Utility(predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata")
        self.house_cat = "wd:Q146"
        self.house_cat_long = 'http://www.wikidata.org/entity/Q146'
        self.egyptian_mau = "wd:Q7295"
        self.egyptian_mau_long = "http://www.wikidata.org/entity/Q7295"


    def test_ancestors(self):
        exclusive_ancestors = self.util.ancestors(self.house_cat)
        inclusive_ancestors = self.util.ancestors(self.house_cat, inclusive=True)

        ## Test that ancestors are not empty (for house_cat)
        self.assertTrue((exclusive_ancestors.length() > 0))

        ## Test that inclusive has one more ancestor
        self.assertEqual(exclusive_ancestors.length() + 1, inclusive_ancestors.length())

        ## Test that exclusive means exclusive
        exclusive_mask = np.in1d([self.house_cat], exclusive_ancestors.concept_list)
        self.assertFalse(exclusive_mask[0])

        ## Test that inclusive means inclusive
        inclusive_mask = np.isin([self.house_cat], inclusive_ancestors.concept_list)
        self.assertTrue(inclusive_mask[0])


    def test_decendents(self):
        exclusive_decendents = self.util.decendents(self.house_cat)
        inclusive_decendents = self.util.decendents(self.house_cat, inclusive=True)

        ## Test that decendents are not empty (for house_cat)
        self.assertTrue((exclusive_decendents.length() > 0))

        ## Test that a leaf has no decendents
        leaf = self.util.decendents(self.egyptian_mau)
        self.assertTrue((leaf.length() == 0))

        ## Test that inclusive has one more decendent
        self.assertEqual(exclusive_decendents.length() + 1, inclusive_decendents.length())

        ## Test that exclusive means exclusive
        exclusive_mask = np.in1d([self.house_cat], exclusive_decendents.concept_list)
        self.assertFalse(exclusive_mask[0])

        ## Test that inclusive means inclusive
        inclusive_mask = np.in1d([self.house_cat], inclusive_decendents.concept_list)
        self.assertTrue(inclusive_mask[0])


    def test_edge_cases(self):
        ## infinity has no ancestors
        self.assertFalse(self.util.ancestors(inf_concept).is_inf())
        self.assertEqual(self.util.ancestors(inf_concept), Concepts([]))

        ## infinity has all decendents
        self.assertTrue(self.util.decendents(inf_concept).is_inf())
        self.assertEqual(self.util.decendents(inf_concept), Concepts.inf())

    def test_time_out(self):
        ## the decendends of root times out
        self.assertTrue(self.util.decendents("wd:Q35120").is_inf())

if __name__ == '__main__':
    unittest.main()
