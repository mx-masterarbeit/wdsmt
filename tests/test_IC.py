import pytest
import numpy as np
from wdsmt.classes.InformationContent import InformationContent


house_cat = "wd:Q146"
egyptian_mau = "wd:Q7295"
dog = "wd:Q144"
tiger = "wd:Q19939"

@pytest.mark.asyncio
async def test_IC_naive():
    information_content = InformationContent(predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata")
    ic_score_cat = await information_content.IC_naive(house_cat)
    ic_score_egyptian_mau = await information_content.IC_naive(egyptian_mau)


    ## Test that IC_naive is greater zero (for house_cat)
    assert ic_score_cat > 0

    ## Test that egyptian_mau has a higher IC than house_cat
    assert ic_score_egyptian_mau > ic_score_cat

@pytest.mark.asyncio
async def test_IC_log_naive():
    information_content = InformationContent(predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata")
    ic_score_cat = await information_content.IC_log_naive(house_cat)
    ic_score_egyptian_mau = await information_content.IC_log_naive(egyptian_mau)


    ## Test that IC_naive is greater zero (for house_cat)
    assert ic_score_cat > 0

    ## Test that egyptian_mau has a higher IC than house_cat
    assert ic_score_egyptian_mau > ic_score_cat
