import pytest
import numpy as np
from wdsmt.classes.Concepts import Concepts

infinite = Concepts.infinite()
abc = Concepts(["A","B","C"])
bcd = Concepts(["B","C", "D"])

def test_length():
    assert infinite.length() == np.inf
    assert abc.length() == 3

def test_union():
    bc = Concepts(["B","C"])
    abcd = Concepts(["A","B","C","D"])

    assert abc.union(bcd).length() == 4
    assert abc.union(bcd) == abcd

    assert infinite.union(bcd).length() == np.inf
    assert abc.union(infinite) == Concepts.infinite()


def test_union():
    bc = Concepts(["B","C"])
    abcd = Concepts(["A","B","C","D"])

    assert abc.union(bcd).length() == 4
    assert abc.union(bcd) == abcd

    assert infinite.union(bcd).length() == np.inf
    assert abc.union(infinite) == Concepts.infinite()

def test_intersection():
    bc = Concepts(["B","C"])

    assert abc.intersection(bcd).length() == 2
    assert abc.intersection(bcd) == bc

    assert infinite.intersection(bcd).length() == 3
    assert abc.intersection(infinite) == abc

def test_setDiff():
    # If A and B are sets, the set difference of A and B is the set of elements in A but not in B.
    ## Regular Case - setdiff of abc and bcd -> expected A
    assert abc.setdiff(bcd) == Concepts(["A"])
    ## Regular Case - setdiff of bcd and abc -> expected D
    assert bcd.setdiff(abc) == Concepts(["D"])
    ## Regular Case - setdiff of abc and [] -> expected abc
    assert abc.setdiff(Concepts([])) == abc
    ## Regular Case - setdiff of [] and abc  -> expected []
    assert Concepts([]).setdiff(abc) == Concepts([])
    ## Regular Case - setdiff of [] and []  -> expected []
    assert Concepts([]).setdiff(Concepts([])) == Concepts([])


    ## EdgeCase - setdiff of infinite and abc -> ecpected infinite:
    assert infinite.setdiff(abc) == infinite
    ## EdgeCase - setdiff of abc and infinite -> ecpected []:
    assert abc.setdiff(infinite) == Concepts([])
    ## EdgeCase - setdiff of infinite and infinite -> ecpected []:
    assert infinite.setdiff(infinite) == Concepts([])
