import numpy as np

from .InformationContent import InformationContent


class SemanticMeasureToolkit(InformationContent):
    def __init__(self, predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata"):
        super.__init__(predicates, endpoint)

    def distance_to_similarity(distance: float):
        return 1 / (distance + 1)

    def similarity_CMatch(self, u, v) -> float:
        """
        CMatch is decscribed by:
        .. math::
            \\mathit{sim}_\text{CMatch}(u,v) = \\frac{|A(u) \\cap A(v)|}{|A(u) \\cup A(v)|}

        with A(c) as the set of ancestors of c going up to the root element.
        """
        A_u, A_v = self.ancestors(u), self.ancestors(v)
        union = np.union1d(A_u, A_v)
        intersection = np.intersect1d(A_u, A_v)

        return len(intersection) / len(union)

    def similarity_Bulskov(self, u, v, predicates, alpha=0.5):
        """
        Bulskov-Similarity between u, v is decscribed by:
        .. math::
            \\mathit{sim}_\text{Bulskov}(u,v) = \\alpha\\frac{|A(u) \\cup A(v)|}{|A(u)|} + (1 - \\alpha)\\frac{|A(u) \\cup A(v)|}{|A(v)|}

        with A(c) as the set of ancestors of c going up to the root element.
        """
        A_u, A_v = self.ancestors(u), self.ancestors(v)
        union = np.union1d(A_u, A_v)

        return alpha * (len(union) / len(A_u)) + (1 - alpha) * (len(union) / len(A_v))

    def distance_Sanchez(self, u, v):
        A_u, A_v = self.ancestors(u), self.ancestors(v)

        diff_u_v, diff_v_u = len(np.setdiff1d(A_u, A_v)), len(np.setdiff1d(A_v, A_u))
        intersection = len(np.intersect1d(A_u, A_v))

        dist = 1 + ((diff_u_v + diff_v_u) / (diff_u_v + diff_v_u + intersection))

        return np.log2(dist)

    def similarity_Sanchez(self, u, v):
        return self.distance_to_similarity(self.distance_Sanchez(u, v))

    def similarity_RE(self, u, v, alpha=0.5):
        A_u, A_v = self.ancestors(u), self.ancestors(v)

        diff_u_v, diff_v_u = len(np.setdiff1d(A_u, A_v)), len(np.setdiff1d(A_v, A_u))
        intersection = len(np.intersect1d(A_u, A_v))

        return intersection / (
            (alpha * diff_u_v) + ((1 - alpha) * diff_v_u) + intersection
        )

    def similarity_Resnik(self, u, v):
        return self.MICA(u, v)[1]

    def similarity_Faith(self, u, v, ic=None):
        if ic is None:
            ic = self.IC_naive

        mica = self.MICA(u, v, ic)[1]

        return mica / (ic(u) + ic(v) - mica)

    def similarity_Lin(self, u, v, ic=None):
        if ic is None:
            ic = self.IC_naive

        mica = self.MICA(u, v, ic)[1]

        return (2 * mica) / (ic(u) + ic(v))

    def distance_JC(self, u, v, ic=None):
        if ic is None:
            ic = self.IC_naive

        mica = self.MICA(u, v, ic)[1]
        return ic(u) + ic(v) - (2 * mica)

    def similarity_JC(self, u, v, ic=None):
        return self.distance_to_similarity(self.distance_JC(u, v, ic))

    def similarity_NUnivers(self, u, v, ic=None):
        if ic is None:
            ic = self.IC_naive

        return self.MICA(u, v, ic)[1] / max(ic(u), ic(v))

    def dinstance_PSec(self, u, v, ic=None):
        if ic is None:
            ic = self.IC_naive

        return 3 * self.MICA(u, v, ic)[1]
