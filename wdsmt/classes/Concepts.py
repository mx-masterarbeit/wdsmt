import numpy as np

infinity_concept = "wd:inf"
inf_concept = infinity_concept


class Concepts:
    """Wrapper Object for Conceptlists with handling for edge cases"""

    def __init__(self, concept_list):
        self.concept_list = np.array(concept_list)

    def __str__(self):
        return str(self.concept_list)

    def __getitem__(self, index):
        return self.concept_list[index]

    @classmethod
    def infinite(cls):
        """
        Create an infinite concept for edge case handling.
        """
        return cls([infinity_concept])

    @classmethod
    def inf(cls):
        """
        Short-hand for infinite
        """
        return cls.infinite()

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return np.array_equal(self.concept_list, other.concept_list)
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def is_infinite(self):
        """
        Check if a Concept is an infinity concept (edge case)
        """
        return (len(self.concept_list) == 1) and (
            self.concept_list[0] == infinity_concept
        )

    def is_inf(self):
        """
        Short-hand for is_infinite()
        """
        return self.is_infinite()

    def length(self):
        """
        Because len(float) throws an error I decided to use the non pythonic way
        """
        if self.is_infinite():
            return np.inf
        else:
            return len(self.concept_list)

    def union(self, other):
        """
        Wrapper function for np.union1d() with handling for timeouted queries
        > In set theory, the union (denoted by ∪) of a collection of sets is the set of all elements in the collection.

        !ToDo: add inplace option
        """
        if self.is_infinite() or other.is_infinite():
            return Concepts.infinite()
        else:
            return Concepts(np.union1d(self.concept_list, other.concept_list))

    def intersection(self, other):
        """
        Wrapper function for np.intersection1d() with handling for timeouted queries.
        > In mathematics, the intersection of two sets A and B, denoted by A ∩ B, is the set containing all elements of A that also belong to B.

        !ToDo: add inplace option
        """
        if self.is_infinite():
            return other
        elif other.is_infinite():
            return self
        else:
            return Concepts(np.intersect1d(self.concept_list, other.concept_list))

    def setdiff(self, other):
        """
        Wrapper function for np.setdiff1d() with handling for timeouted queries.
        > If A and B are sets, the set difference of A and B is the set of elements in A but not in B.

        !ToDo: add inplace option
        """
        if self.is_infinite() and other.is_infinite():
            return Concepts([])
        elif self.is_infinite():
            return Concepts.infinite()  # infinite minus finite is still infinite
        elif other.is_infinite():
            return Concepts([])
        else:
            return Concepts(np.setdiff1d(self.concept_list, other.concept_list))
