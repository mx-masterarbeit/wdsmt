import asyncio

from ..typings import Similarity

from .Concepts import Concepts
from .GraphAccessor import GraphAccessor


class DirectGroupwiseSimilarity:
    """
    Grouped functions for calculating direct group-wise similarities.
    Direct groupwise similarity functions have one of those signatures:
        - U x V -> float
        - U x V x ic_function-> float

    This class needs to be initiated with predicates and an endpoint.

    `cls.function_list` has all the similarity functions available without information content
    `cls.ic_function_list` has all the similarity functions available with information content
    """
    def __init__(self, predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata"):
        self.predicates, self.endpoint = predicates, endpoint

    function_list = ['term_overlap', 'normalized_term_overlap']
    ic_funtion_list = ['group_IC']

    async def _get_ancestor_union(self, concepts: Concepts):
        """
        Helper function to access all ancestors of all given concepts
        """
        concepts = Concepts(concepts)

        if concepts.is_inf():
            return Concepts.infinite()

        ancestors = []
        async with GraphAccessor(self.predicates, self.endpoint) as graph:
            ancestors = await asyncio.gather(
                *map(graph.ancestors, concepts),
                return_exceptions=True
            )

        ancestor_union = Concepts([])
        for ancestor in ancestors:
            ancestor_union = ancestor_union.union(ancestor)

        return ancestor_union

    async def term_overlap(self, U, V):
        """
        The term overlap similarity describes the relationship of the ancestors both in U and V to
        all ancestors. (See Thesis p.14)
        """
        [C_U, C_V] = await asyncio.gather(
            self._get_ancestor_union(U),
            self._get_ancestor_union(V),
            return_exceptions=True
        )

        return C_U.intersection(C_V).length() / C_U.union(C_V).length()

    async def normalized_term_overlap(self, U, V):
        """
        The normalized term overlap similarity describes the relationship of the ancestors both in
        U and V to the smaller set of ancestors of U and V. (See Thesis p.14)
        """
        [C_U, C_V] = await asyncio.gather(
            self._get_ancestor_union(U),
            self._get_ancestor_union(V),
            return_exceptions=True
        )

        return C_U.intersection(C_V).length() / min(C_U.length(), C_V.length())

    async def group_IC(self, U, V, ic):
        """
        Group Information Content computes the relative groupwise information content.
        (See Thesis p.17)
        """
        [C_U, C_V] = await asyncio.gather(
            self._get_ancestor_union(U),
            self._get_ancestor_union(V),
            return_exceptions=True
        )

        intersection_sum = union_sum = 0

        # could be solved with gather
        for c in C_U.intersection(C_V):
            intersection_sum += await ic(c)

        # could be solved with gather
        for c in C_U.union(C_V):
            union_sum += await ic(c)

        return intersection_sum / union_sum

    # def funtion_list():
    #     return ['term_overlap', 'normalized_term_overlap', 'group_IC']
