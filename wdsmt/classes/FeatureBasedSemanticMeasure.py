import asyncio
import numpy as np

from .GraphAccessor import GraphAccessor
from ..typings import Distance, Similarity


class FeatureBasedSemanticMeasure:
    """
    Concept feature analysis focuses on looking at concepts as a set of features. (See Thesis p.14)

    This class needs to be initiated with predicates and an endpoint.
    """

    def __init__(self, predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata"):
        self.predicates, self.endpoint = predicates, endpoint

    async def _get_ancestors(self, u, v):
        """
        Helper function to load the ancestors of two concepts from the endpoint.
        """
        A_u = A_v = {}
        async with GraphAccessor(self.predicates, self.endpoint) as graph:
            [A_u, A_v] = await asyncio.gather(
                graph.ancestors(u, inclusive=True),
                graph.ancestors(v, inclusive=True),
                return_exceptions=False,
            )

        return (A_u, A_v)

    async def similarity_CMatch(self, u: str, v: str) -> Similarity:
        """
        CMatch is the naive implementation in that category. The concept match algorithm looks as the number of ancestors the two concepts u, v have in common.
        .. math::
            \mathit{sim}_\text{CMatch}(u,v) = \frac{|A(u) \\cap A(v)|}{|A(u) \\cup A(v)|}

        with A(c) as the set of ancestors of c going up to the root element.
        """
        (A_u, A_v) = await self._get_ancestors(u, v)
        union = A_u.union(A_v)
        intersection = A_u.intersection(A_v)

        return intersection.length() / union.length()

    async def similarity_Bulskov(self, u: str, v: str, alpha=0.5) -> Similarity:
        """
        Bulskov introduced a weighted algorithm which combines the ancestors u and v, while allowing to weight one of them higher.
        .. math::
            \mathit{sim}_\text{Bulskov}(u,v) = \alpha\frac{|A(u) \cup A(v)|}{|A(u)|} + (1 - \alpha)\frac{|A(u) \cup A(v)|}{|A(v)|}

        with A(c) as the set of ancestors of c going up to the root element.
        """
        (A_u, A_v) = await self._get_ancestors(u, v)
        union = A_u.union(A_v)

        return alpha * (union.length() / A_u.length()) + (1 - alpha) * (union.length() / A_v.length())

    async def similarity_RE(self, u: str, v: str, alpha=0.5) -> Similarity:
        """
        RE is named after the two authors Rodrígues and Egenhofer. It compares the ancestors u and v have in common with those they do not have in common.
        .. math::
            \mathit{sim}_\text{RE}(u,v) = \frac{|A(u) \cap A(v)|}{\alpha \cdot |A(u) \setminus A(v)| + (1 - \alpha) \cdot |A(v) \setminus A(u)| + |A(u) \cap A(v)|}
        """
        (A_u, A_v) = await self._get_ancestors(u, v)

        diff_u_v, diff_v_u = A_u.setdiff(A_v).length(), A_v.setdiff(A_u).length()
        intersection = A_u.intersection(A_v).length()

        return intersection / (
            (alpha * diff_u_v) + ((1 - alpha) * diff_v_u) + intersection
        )
