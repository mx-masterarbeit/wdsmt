import numpy as np

from aiosparql.client import SPARQLClient, SPARQLRequestFailed

# Caching
from aiocache import cached, Cache
from aiocache.serializers import PickleSerializer

from .Concepts import infinity_concept, Concepts


class GraphAccessor:
    """
    Within the GraphAccessor class are all functions implemented that are needed to access the knowledge graph. A
    GraphAccessor needs to be instantiated with the predicates considered for vertical movement and the endpoint.
    The default predicates are ‘instance of (wdt:P31)’ and ‘subclass of (P279)’ together with the default endpoint
    wikidata.
    !TODO Other endpoints are not implemented yet.
    """
    def __init__(self, predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata"):
        self.timeout = None
        if endpoint == "wikidata":
            self.sparql = SPARQLClient("https://query.wikidata.org/sparql")
            self.timeout = 65

        self.endpoint = endpoint
        self.predicates = predicates

    def __del__(self):
        self.sparql.close()

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.sparql.close()

    def distance_to_similarity(distance: float) -> float:
        return 1 / (distance + 1)

    async def run_query(self, query):
        """
        Runs a given query. Handles TimeOut Exceptions.
        """
        try:
            ret = await self.sparql.query(query)
        except SPARQLRequestFailed:
            # if the query times out return something that will be parsed as infinity
            return {
                "results": {
                    "bindings": [
                        {
                            "ancestor": {"value": infinity_concept},
                            "ancestorCount": {"value": np.inf},
                            "decendent": {"value": infinity_concept},
                            "decendentCount": {"value": np.inf},
                        }
                    ]
                }
            }
        except Exception as err:
            print("Exception occured ", type(err))
            return err

        return ret

    def build_ancestors_query(self, concept: str) -> str:
        """
        Returns the query for the ancestors of a concept going up to the root element.
        Excluding the concept itself.

        """
        predicates_nl = "\n".join(self.predicates)
        predicates_inline = " | ".join(self.predicates)

        return f"""
        SELECT DISTINCT ?ancestor
        WHERE {{
        VALUES ?pre {{
            {predicates_nl}
        }}
        {concept}  ({predicates_inline})* ?item.
        ?item ?pre ?ancestor.
        FILTER (?ancestor != {concept})
        }}
        """

    def build_ancestors_count_query(self, concept: str) -> str:
        """
        Returns the query for the ancestors of a concept going up to the root element.
        Excluding the concept itself.

        """
        predicates_nl = "\n".join(self.predicates)
        predicates_inline = " | ".join(self.predicates)

        return f"""
        SELECT (count(DISTINCT ?ancestor) as ?ancestorCount)
        WHERE {{
        VALUES ?pre {{
            {predicates_nl}
        }}
        {concept}  ({predicates_inline})* ?item.
        ?item ?pre ?ancestor.
        FILTER (?ancestor != {concept})
        }}
        """

    @cached(
        cache=Cache.REDIS,
        serializer=PickleSerializer(),
        namespace="semantic",
        noself=True,
    )
    async def ancestors(self, concept: str, inclusive=False):
        """
        Returns the set of ancestors of a concept going up to the root element.
        Special Case 1: sparql timeout -> infinite ancestors
        Special Case 2: ancestor of infinity_concept -> no ancestors
        """
        if concept == infinity_concept:
            return Concepts([])

        query = self.build_ancestors_query(concept)

        results = await self.run_query(query)

        ancestors = list(
            [
                ancestor["ancestor"]["value"].replace(
                    "http://www.wikidata.org/entity/", "wd:"
                )
                for ancestor in results["results"]["bindings"]
            ]
        )

        # QUICKFIX
        # handle edge case for elements with no ancestors
        # TODO!: handle this edge case correctly
        if concept == "wd:Q3711790":
            print(len(ancestors))
        if len(ancestors) == 0:
            return Concepts([infinity_concept])

        # The query excludes the concept itself by default therefore it has to be added if needed.
        if inclusive and (ancestors[0] != infinity_concept):
            ancestors.append(concept)

        return Concepts(ancestors)

    @cached(
        cache=Cache.REDIS,
        serializer=PickleSerializer(),
        namespace="semantic",
        noself=True,
    )
    async def ancestor_count(self, concept: str, inclusive=False):
        """
        Returns the set of ancestors of a concept going up to the root element.
        Special Case 1: sparql timeout -> infinite ancestors
        Special Case 2: ancestor of infinity_concept -> no ancestors
        """
        if concept == infinity_concept:
            return 0

        query = self.build_ancestors_count_query(concept)
        results = await self.run_query(query)

        # !TODO Paralleize
        ancestorCount = float(
            results["results"]["bindings"][0]["ancestorCount"]["value"]
        )

        # QUICKFIX
        # handle edge case for elements with no ancestors
        # TODO!: handle this edge case correctly
        if ancestorCount == 0:
            return np.inf

        # The query excludes the concept itself by default therefore it has to be added if needed.
        if inclusive:
            ancestorCount += 1

        return ancestorCount

    def build_decendents_query(self, concept: str) -> str:
        """
        Returns the query for the decendents of a concept going down to the leaves.
        Excluding the concept itself.
        """
        predicates_nl = "\n".join(self.predicates)
        predicates_inline = " | ".join(self.predicates)

        return f"""\
        SELECT DISTINCT ?decendent
        WHERE {{
            VALUES ?pre {{ {predicates_nl} }}
            ?item ({predicates_inline})* {concept}.
            ?decendent ?pre ?item.
            FILTER (?decendent != {concept})
        }}
        """

    def build_decendents_count_query(self, concept: str) -> str:
        """
        Returns the query for the ancestors of a concept going up to the root element.
        Excluding the concept itself.

        """
        predicates_nl = "\n".join(self.predicates)
        predicates_inline = " | ".join(self.predicates)

        return f"""
        SELECT (count(DISTINCT ?decendent) as ?decendentCount)
        WHERE {{
            VALUES ?pre {{ {predicates_nl} }}
            ?item ({predicates_inline})* {concept}.
            ?decendent ?pre ?item.
            FILTER (?decendent != {concept})
        }}
        """

    @cached(
        cache=Cache.REDIS,
        serializer=PickleSerializer(),
        namespace="semantic",
        noself=True,
    )
    async def decendents(self, concept: str, inclusive=False):
        """
        Returns the set of decendents of a concept going down to the leaves.
        Special Case 1: sparql timeout -> infinite decendents
        Special Case 2: decendent of infinity_concept -> infinite decendents
        """
        if concept == infinity_concept:
            return Concepts.infinite()

        query = self.build_decendents_query(concept)

        results = await self.run_query(query)

        decendents = [
            decendent["decendent"]["value"].replace(
                "http://www.wikidata.org/entity/", "wd:"
            )
            for decendent in results["results"]["bindings"]
        ]

        # The query excludes the concept itself by default therefore it has to be added if needed.
        if inclusive and len(decendents) > 0 and (decendents[0] != infinity_concept):
            decendents.append(concept)

        return Concepts(decendents)

    @cached(
        cache=Cache.REDIS,
        serializer=PickleSerializer(),
        namespace="semantic",
        noself=True,
    )
    async def decendent_count(self, concept: str, inclusive=False):
        """
        Returns the set of ancestors of a concept going up to the root element.
        Special Case 1: sparql timeout -> infinite ancestors
        Special Case 2: ancestor of infinity_concept -> no ancestors
        """
        if concept == infinity_concept:
            return 0

        query = self.build_decendents_count_query(concept)
        results = await self.run_query(query)

        # !TODO Paralleize
        decendentCount = float(
            results["results"]["bindings"][0]["decendentCount"]["value"]
        )

        # The query excludes the concept itself by default therefore it has to be added if needed.
        if inclusive:
            decendentCount += 1

        return decendentCount
