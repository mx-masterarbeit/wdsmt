from ..typings import Similarity

from .Concepts import Concepts


class IndirectGroupwiseSimilarity:
    """
    Grouped functions for calculating indirect group-wise similarities.
    All groupwise similarity functions have this signature:
        U x V x pairwise similarity -> float

    `cls.funtion_list` has all the similarity functions available
    """
    function_list = ['average', 'max_average', 'best_match_max', 'best_match_average']

    async def average(U, V, sim) -> Similarity:
        """
        A naive average of the sum of all pair-wise similarities between two groups.
        """
        numerator = 0
        for u in U:
            for v in V:
                numerator += await sim(u, v)

        return numerator / (len(U) * len(V))

    async def max_average(U, V, sim) -> Similarity:
        """
        The averaged maximum of all pair-wise similarities between two groups.
        """
        numerator = 0
        for u in U:
            max_u = []
            for v in V:
                max_u.append(await sim(u, v))

            numerator += max(max_u)

        return numerator / len(U)

    async def best_match_max(U, V, sim) -> Similarity:
        """
        An extension of the `max_average` algorithm that uses the maximum
        of the two possible arrangements.
        """
        return max(await IndirectGroupwiseSimilarity.max_average(U, V, sim), await IndirectGroupwiseSimilarity.max_average(V, U, sim))

    async def best_match_average(U, V, sim) -> Similarity:
        """
        An extension of the `max_average` algorithm that averages the results of the two possible arrangements.
        """
        return (await IndirectGroupwiseSimilarity.max_average(U, V, sim) + await IndirectGroupwiseSimilarity.max_average(V, U, sim)) / 2
