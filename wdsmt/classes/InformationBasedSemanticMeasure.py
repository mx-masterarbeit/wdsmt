import asyncio
from operator import itemgetter

from .GraphAccessor import GraphAccessor
from .InformationContent import InformationContent
from ..typings import Distance, Similarity


class InformationBasedSemanticMeasure:
    """
    All measures based on information theoretical analysis depend on the function of information content (IC) and its respective function for the most informative common ancestor (MICA). It is a convention that MICA uses the same IC-function as the similarity algorithm. (See Thesis p.17)
    """

    def __init__(
        self,
        predicates=["wdt:P31", "wdt:P279"],
        endpoint="wikidata",
        ic_function="IC_naive",
        semaphore_limit=4,
    ):
        self.information_content = InformationContent(predicates, endpoint)
        self.predicates, self.endpoint = predicates, endpoint
        self.semaphore_limit = semaphore_limit
        if ic_function == "IC_log_naive":
            self.ic = self.information_content.IC_log_naive
        else:
            self.ic = self.information_content.IC_naive

    def _distance_to_similarity(self, distance: float):
        return 1 / (distance + 1)

    async def MICA(self, u: str, v: str):
        """
        Returns the Most Informative Common Ancestor (MICA) of concepts u and v using the given function for the Information Content.
        """

        async def gather_ic_tuples(*ancestors):
            semaphore = asyncio.Semaphore(self.semaphore_limit)

            async def get_ic_tuple(common_ancestor):
                async with semaphore:
                    return (common_ancestor, await self.ic(common_ancestor))

            return await asyncio.gather(
                *(get_ic_tuple(ancestor) for ancestor in ancestors),
                return_exceptions=True
            )

        A_u, A_v = None, None
        async with GraphAccessor(self.predicates, self.endpoint) as graph:
            [A_u, A_v] = await asyncio.gather(
                graph.ancestors(u, inclusive=True),
                graph.ancestors(v, inclusive=True),
                return_exceptions=True,
            )

        intersection = A_u.intersection(A_v).concept_list
        ica = await gather_ic_tuples(*intersection)
        mica = max(ica, key=itemgetter(1))

        return {"value": mica[0], "ic_score": mica[1]}

    async def similarity_Resnik(self, u: str, v: str) -> Similarity:
        """
        Resnik is the naive measure using information content of the most informative common ancestor.
        """
        mica_tuple = await (self.MICA(u, v))
        return mica_tuple["ic_score"]

    async def similarity_Faith(self, u: str, v: str) -> Similarity:
        """
        Faith uses the information content of the most informative common ancestor together with the information content of the two concepts in question. (See Thesis p.17)
        """
        mica = (await self.MICA(u, v))["ic_score"]
        [ic_u, ic_v] = await asyncio.gather(self.ic(u), self.ic(v))

        # ISSUE:
        # if ic_u + ic_v == mica => divide by zero
        # all inf or all 0 are trivial cases
        try:
            return mica / (ic_u + ic_v - mica)
        except ZeroDivisionError:
            return 0

    async def similarity_Lin(self, u: str, v: str) -> Similarity:
        """
        Lin uses the information content of the most informative common ancestor together with the information content of the two concepts in question. (See Thesis p.17)
        """
        mica = (await self.MICA(u, v))["ic_score"]
        [ic_u, ic_v] = await asyncio.gather(self.ic(u), self.ic(v))

        # ISSUE:
        # if ic_u = ic_v = 0 => divide by zero
        try:
            return (2 * mica) / (ic_u + ic_v)
        except ZeroDivisionError:
            return 0

    async def similarity_NUnivers(self, u: str, v: str) -> Similarity:
        """
        NUnivers uses the information content of the most informative common ancestor together with the information content of the two concepts in question. (See Thesis p.17)
        """
        mica = (await self.MICA(u, v))["ic_score"]
        [ic_u, ic_v] = await asyncio.gather(self.ic(u), self.ic(v))
        # ISSUE:
        # if ic_u = ic_v = 0 => divide by zero
        try:
            return mica / max(ic_u, ic_v)
        except ZeroDivisionError:
            return 0
