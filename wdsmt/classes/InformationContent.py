import asyncio

import numpy as np

from .GraphAccessor import GraphAccessor
from .Concepts import inf_concept


class InformationContent:
    """
    Information content, or IC, describes the degree of abstraction of a concept. The more specific
    a concept is the higher the information content should be. (See Thesis p.15)

    This class needs to be initiated with predicates and an endpoint.
    """
    def __init__(self, predicates=["wdt:P31", "wdt:P279"], endpoint="wikidata"):
        self.predicates = predicates
        self.endpoint = endpoint

    async def _get_counts(self, concept):
        """
        Helper function to count the number of ancestors and decendents.
        """
        async with GraphAccessor(self.predicates, self.endpoint) as graph:
            [ancestor_count, decendent_count] = await asyncio.gather(
                graph.ancestor_count(concept),
                graph.decendent_count(concept),
                return_exceptions=True,
            )
            return (ancestor_count, decendent_count)

    async def IC_aps(self, concept, alpha=0.5):
        """
        For the a-preriori-score (APS) the assumption is, that a concept is more specific if it has
        only few decendents. (See Thesis p.15)
        ..math::
            \mathit{IC}_\text{APS}(c) = \frac{1}{|D(c)| + 2}
        """
        len_A_c, len_D_c = await self._get_counts(concept)
        return 1 / (len_D_c + 2)

    async def IC_naive(self, concept: str, alpha=0.5) -> float:
        """
        Inspired by the a-priori-score mentioned in \\cite{SchickelZuber2007OSS} and Similarity Measures using ancestors this
        algorithm combines ancestors and decendents to give naive information content score as a fallback.
        (See Thesis p.16)
        ..math::
            \mathit{IC}_\text{naive}(c) = \alpha \cdot \left(1-\frac{1}{|A(c)| + 1}\left) + (1-\alpha) \cdot \frac{1}{|D(c)| + 1}

        with A(c) as the set of ancestors and D(c) as the set of decendents of c.

        Special Case 1: concept == infinity -> IC := -1
        """
        if concept == inf_concept:
            return -1

        len_A_c, len_D_c = await self._get_counts(concept)

        if (len_A_c == np.inf) or (len_D_c == np.inf):
            return 0

        # print(f"|A({concept})| = {len_A_c}")
        # print(f"|D({concept})| = {len_D_c}")

        ancestor_part = 1 - (1 / (len_A_c + 1))
        decendent_part = 1 / (len_D_c + 1)

        return alpha * ancestor_part + (1 - alpha) * decendent_part

    async def IC_log_naive(self, concept: str, alpha=0.5) -> float:
        """
        Inspired by the a-priori-score mentioned in \\cite{SchickelZuber2007OSS} and Similarity Measures using ancestors this
        algorithm combines ancestors and decendents to give naive information content score as a fallback.
        (See Thesis p.16)
        .. math::
            \mathit{IC}_{\log\text{naive}}(c) = \alpha \cdot \left(1-\frac{1}{|A(c)| + 1}\left) + (1-\alpha) \cdot \frac{1}{|D(c)| + 1}

        with A(c) as the set of ancestors and D(c) as the set of decendents of a concept c.

        Special Case 1: concept == infinity -> IC := -1
        """
        if concept == inf_concept:
            return -1

        len_A_c, len_D_c = await self._get_counts(concept)
        # len_A_c, len_D_c = np.log2(self.ancestor_count(concept) + 1), np.log2(self.decendent_count(concept) + 1)

        if (len_A_c == np.inf) or (len_D_c == np.inf):
            return 0

        ancestor_part = 1 - (1 / (np.log2(len_A_c + 1) + 1))
        decendent_part = 1 / (np.log2(len_D_c + 1) + 1)

        return alpha * ancestor_part + (1 - alpha) * decendent_part
