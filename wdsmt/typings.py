from nptyping import NDArray

Concept = str
Concepts = NDArray[Concept]

Predicate = str
Predicates = NDArray[str]

Distance = float
Similarity = float
